﻿using Microsoft.AspNet.SignalR.Messaging;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.SignalR
{
    public class CustomersHub : Hub
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersHub(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository=customerRepository;
            _preferenceRepository=preferenceRepository;
        }

        public async Task GetAllAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            await Clients.All.SendAsync("GetAllAsync", JsonConvert.SerializeObject(response));
        }
        public async Task GetByIdAsync(string id)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(id));

            var response = new CustomerResponse(customer);

            await Clients.All.SendAsync("GetByIdAsync", JsonConvert.SerializeObject(response));
        }

        public async Task CreateCustomerAsync()
        {
            var request = new CreateOrEditCustomerRequest
            {
                Email="test@mail.ru",
                FirstName ="Вася",
                LastName = "Петров",
                PreferenceIds = new List<Guid> { Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84") }
            };
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);
            await GetByIdAsync(customer.Id.ToString());

           // await Clients.All.SendAsync("CreateCustomerAsync", JsonConvert.SerializeObject(customer));
        }


        public async Task Send(string message)
        {
            await Clients.All.SendAsync(message);
        }
    }
}
