﻿using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.SignalR
{
    public interface IHubClient
    {
        Task BroadcastMessage(string name, string message);
    }
}
