﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.SignalR;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("CustomersHub")]
    public class CustomersSignalRController : ControllerBase
    {
        private readonly IHubContext<CustomersHub> _hubContext;
        private readonly IRepository<Customer> _customerRepository;
        public CustomersSignalRController(IHubContext<CustomersHub> hubContext, IRepository<Customer> customerRepository)
        {
            _hubContext=hubContext;
            _customerRepository=customerRepository;
        }

        //[HttpGet]
        //public async Task GetCustomersAsync()
        //{
        //    var customers = await _customerRepository.GetAllAsync();

        //    var response = customers.Select(x => new CustomerShortResponse()
        //    {
        //        Id = x.Id,
        //        Email = x.Email,
        //        FirstName = x.FirstName,
        //        LastName = x.LastName
        //    }).ToList();

        //    await _hubContext.Clients.All.SendAsync("message", JsonConvert.SerializeObject(response));

        //}
    }
}
