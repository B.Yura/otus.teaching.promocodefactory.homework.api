﻿using Microsoft.AspNet.SignalR.Infrastructure;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.ConsoleTestMessage
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine(@"введите комманды:
                                all=> получить всех
                                byId => получить по id
                                create => создать");

            var hubConnection = new HubConnectionBuilder()
                .WithUrl($"http://localhost:5000/customers")
                .Build();

            hubConnection.On("GetAllAsync", (string data) =>
            {
                Console.WriteLine(data);
            });

            hubConnection.On("GetByIdAsync", (string data) =>
            {
                Console.WriteLine(data);
            });

            hubConnection.On("CreateCustomerAsync", (string data) =>
            {
                Console.WriteLine(data);
            });

            await hubConnection.StartAsync();

            while (true)
            {
                var message = Console.ReadLine();            

                switch (message)
                {
                    case "all":
                         await hubConnection.InvokeAsync("GetAllAsync");
                        break;
                    case "byId":
                        await hubConnection.InvokeAsync("GetByIdAsync", "a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                        break;
                    case "create":
                        await hubConnection.InvokeAsync("CreateCustomerAsync");
                        break;
                    default:
                        Console.WriteLine("Неверная команда");
                        break;
                }              
            }

        }
    }
}
